import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    savedMedicalHistory: {
      currentDate: new Date(),
      time: '',
      symptoms: '',
      currentHistory: '',
      pastHistory: '',
      familyHistory: '',
      personalHistory: ''
    },
    savePhysicalExamination: {
      temp: '',
      pulse: '',
      respirationRate: '',
      bp: '',
      height: '',
      weight: '',
      bmi: '',
      painScore: '',
      painfulArea: '',
      detailedPhysicalExamination: ''
    },
    saveMedicalDiagnosis: {
      elementaryPrinciples: '',
      seasonalPrinciples: '',
      agePrinciples: '',
      timePrinciples: '',
      geographicalPlaceOfBirth: '',
      geographicalPresentAddress: '',
      causeOfSymptoms: '',
      summaryOfSickness: '',
      diagnosisBasedOfTheFourElements: '',
      diseaseDiagnosis: '',
      modernMedicalDiagnosis: ''
    },
    saveTreatment: {
      treatmentPlan: '',
      thaiHerbalCompressMassage: '',
      herbalSteam: '',
      operative: '',
      evaluationAfterTreatments: '',
      painScoreAf: '',
      examinationDetails: '',
      suggestions: '',
      appointmentDate: ''
    },
    saveMedicinePrescription: {
      type: '',
      drugNumber: '',
      drugName: '',
      amount: 0
    }
  },
  mutations: {
    updateMHValue(state, payload) {
      state.savedMedicalHistory = payload;
    },
    updatePhyEValue(state, payload) {
      state.savePhysicalExamination = payload;
    },
    updateMDValue(state, payload) {
      state.saveMedicalDiagnosis = payload;
    },
    updateTMValue(state, payload) {
      state.saveTreatment = payload;
    },
    updateMPValue(state, payload) {
      state.saveMedicinePrescription = payload;
    }
  },
  actions: {
  },
  modules: {
  }
})
