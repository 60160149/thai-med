import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/customers/create',
    name: 'customers.create',
    component: () => import('@/views/customer/Create')
  },
  {
    path: '/customers',
    name: 'customers',
    component: () => import('@/views/customer/Index')
  },
  {
    path: '/customers/:id',
    name: 'customers.edit',
    component: () => import('@/views/customer/Edit')
  },
  {
    path: '/customers/:id',
    name: 'customers.detail',
    component: () => import('@/views/customer/Detail')
  },
  {
    path: '/medicalHistory/create/customer/:id',
    name: 'medicalHistory.create',
    component: () => import('@/views/medicalhistory/Create')
  },
  {
    path: '/medicalHistory/customer/:id',
    name: 'medicalHistory',
    component: () => import('@/views/medicalhistory/Index')
  },
  {
    path: '/medicalHistory/:id/customer/:customerId',
    name: 'medicalHistory.edit',
    component: () => import('@/views/medicalhistory/Edit')
  },
  {
    path: '/medicalHistory/detail/:id/customer/:customerId',
    name: 'medicalHistory.detail',
    component: () => import('@/views/medicalhistory/Detail'),
    props: true
  },
  {
    path: '/drug',
    name: 'drug',
    component: () => import('@/views/drug/Index')
  },
  {
    path: '/drug/create',
    name: 'drug.create',
    component: () => import('@/views/drug/Create')
  }
  ,
  {
    path: '/drug/detail/:id',
    name: 'drug.detail',
    component: () => import('@/views/drug/Detail')
  },
  {
    path: '/drug/:id',
    name: 'drug.edit',
    component: () => import('@/views/drug/Edit')
  },
  {
    path: '/physicalExamination/customer/:id',
    name: 'physicalExamination',
    component: () => import('@/views/physicalExamination/Index')
  },
  {
    path: '/physicalExamination/create/customer/:id',
    name: 'physicalExamination.create',
    component: () => import('@/views/physicalExamination/Create')
  },
  {
    path: '/physicalExamination/edit/:id/customer/:customerId',
    name: 'physicalExamination.edit',
    component: () => import('@/views/physicalExamination/Edit')
  },
  {
    path: '/physicalExamination/detail/:id/customer/:customerId',
    name: 'physicalExamination.detail',
    component: () => import('@/views/physicalExamination/Detail')
  },
  {
    path: '/medicalCertificate/create/customer/:id',
    name: 'medicalCertificate.create',
    component: () => import('@/views/MedicalCertificate')
  },
  {
    path: '/medicalDiagnosis/customer/:id',
    name: 'medicalDiagnosis',
    component: () => import('@/views/medicalDiagnosis/Index')
  },
  {
    path: '/medicalDiagnosis/create/customer/:id',
    name: 'medicalDiagnosis.create',
    component: () => import('@/views/medicalDiagnosis/Create')
  },
  {
    path: '/medicalDiagnosis/edit/:id/customer/:customerId',
    name: 'medicalDiagnosis.edit',
    component: () => import('@/views/medicalDiagnosis/Edit')
  },
  {
    path: '/medicalDiagnosis/detail/:id/customer/:customerId',
    name: 'medicalDiagnosis.detail',
    component: () => import('@/views/medicalDiagnosis/Detail')
  },
  {
    path: '/treatmentPlan/customer/:id',
    name: 'treatmentPlan',
    component: () => import('@/views/treatmentPlan/Index')
  },
  {
    path: '/treatmentPlan/create/customer/:id',
    name: 'treatmentPlan.create',
    component: () => import('@/views/treatmentPlan/Create')
  },
  {
    path: '/treatmentPlan/:id/edit/customer/:customerId',
    name: 'treatmentPlan.edit',
    component: () => import('@/views/treatmentPlan/Edit')
  },
  {
    path: '/treatmentPlan/detail/:id/customer/:customerId',
    name: 'treatmentPlan.detail',
    component: () => import('@/views/treatmentPlan/Detail')
  },
  {
    path: '/user',
    name: 'user',
    component: () => import('@/views/user/Index')
  },
  {
    path: '/user/create',
    name: 'user.create',
    component: () => import('@/views/user/Create')
  },
  {
    path: '/user/edit/:id',
    name: 'user.edit',
    component: () => import('@/views/user/Edit')
  },
  {
    path: '/user/edit/pass/:id',
    name: 'user.editPass',
    component: () => import('@/views/user/EditPass')
  },
  {
    path: '/medicinePrescription/customer/:id',
    name: 'medicinePrescription',
    component: () => import('@/views/medicinePrescription/Index')
  },
  {
    path: '/medicinePrescription/create/customer/:id',
    name: 'medicinePrescription.create',
    component: () => import('@/views/medicinePrescription/Create')
  },
  {
    path: '/medicinePrescription/detail/:id/customer/:customerId',
    name: 'medicinePrescription.detail',
    component: () => import('@/views/medicinePrescription/Detail')
  },
  {
    path: '/medicinePrescription/edit/:id/customer/:customerId',
    name: 'medicinePrescription.edit',
    component: () => import('@/views/medicinePrescription/Edit')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
