import axios from "axios"

const instance = axios.create({
  baseURL: "http://prepro.informatics.buu.ac.th:3333/",
})

instance.interceptors.request.use(request => {
  let user = JSON.parse(localStorage.getItem('user'));

  if (user && user.token) {
    request.headers["Authorization"] = `Bearer ${user.token}`;
  }
  console.log("Request", request)
  return request
})

instance.interceptors.response.use(response => {
  console.log("Response", response)
  return response
})

export default instance